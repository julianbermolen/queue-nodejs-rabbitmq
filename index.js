//libreria utilizada para realizar las comunicaciones con rabbitmq
const amqp = require("amqplib");

//Para realizar la conexión, precisamos enviar ciertas configuraciones
const rabbitSettings = {
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'bermo',
    password: 'bermo',
    vhost: '/',
    authMechanism: ['PLAIN','AMQPLAIN','EXTERNAL']
}

connect();

//Estableceremos la conexión, crearemos un canal y crearemos una cola de mensajes sobre empleados
async function connect(){

    const queue = "empleados";

    const msgs = [
        {"name":"Julian","years":26,"description":"Developer"},
        {"name":"Julian","years":24,"description":"Analyst"}
    ] ;

    try {
        const conn = await amqp.connect(rabbitSettings);
        console.log("Conexión a RabbitMQ Creada");

        const channel = await conn.createChannel();
        console.log("Canal creado");

        const res = await channel.assertQueue(queue);
        console.log("Cola de mensajes Empleados creada");

        for(let msg in msgs){
            await channel.sendToQueue(queue, Buffer.from(JSON.stringify(msgs[msg])));
            console.log(`Mensaje enviado: ${msg}`);
        }

    } catch (error) {
        console.error(`error -> ${error}`);
    }
}