const amqp = require("amqplib");

const rabbitSettings = {
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'bermo',
    password: 'bermo',
    vhost: '/',
    authMechanism: ['PLAIN','AMQPLAIN','EXTERNAL']
}

connect();

async function connect(){

    const queue = "empleados";

    try {
        const conn = await amqp.connect(rabbitSettings);
        console.log("Conexión a RabbitMQ Creada");

        const channel = await conn.createChannel();
        console.log("Canal creado");

        const res = await channel.assertQueue(queue);
        console.log("Cola de mensajes Empleados creada");

        channel.consume(queue, message =>{
            let empleado = JSON.parse(message.content.toString());
            console.log(`Mensaje recibido: ${empleado.name}`);
            console.log(empleado);

//Eliminar mensaje de cola
            if(empleado.description == 'Developer'){
                channel.ack(message);
                console.log('Deleted message from queue');
            }else{
                console.log(`Este mensaje no lo borro, ya que es util para algien más: ${empleado}`);
            }
        })

    } catch (error) {
        console.error(`error -> ${error}`);
    }
}